import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageSplitter
{
	public static final String imageFile = "darkwood.jpg";
	public static final String output = "dark/texture";

	public static final int width = 450;
	public static final int height = 450;

	public static void main(String[] args) throws IOException
	{
		BufferedImage image = ImageIO.read(new File(imageFile));

		// Get number of columns and rows based on size of image and size of required tiles
		int columns = image.getHeight() / height;
		int rows = image.getWidth() / width;

		System.out.println("rows: " + rows);
		System.out.println("columns: " + columns);

		System.out.println("Number of images: " + rows * columns);

		int imageNumber = 0;

		for (int y = 0; y < columns; y++)
		{
			for (int x = 0; x < rows; x++)
			{
				BufferedImage temp = new BufferedImage(width, height, image.getType());

				Graphics2D graphics = temp.createGraphics();
				
				int firstCornerX = width * x;
				int firstCornerY = height * y;
				int secondCornerX = (width * x) + width;
				int secondCornerY = (height * y) + height;

				graphics.drawImage(image, 0, 0, width, height, firstCornerX, firstCornerY, secondCornerX, secondCornerY, null);
				graphics.dispose();

				ImageIO.write(temp, "jpg", new File(output + imageNumber + ".jpg"));
				imageNumber++;
			}
		}
	}
}
