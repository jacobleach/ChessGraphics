/**
 * Static methods to load lights from config file
 *
 * @author leach.jacob@gmail.com (Jacob Leach)
 */

/**
 *
 *
 * @param {THREE.Scene} the scene to add the lights to
 */
var addLightsFromConfig = function(scene) {
    addDirectionalFromConfig(scene);
    addAmbientFromConfig(scene);
    addSpotFromConfig(scene);
}

/**
 *
 *
 * @param {THREE.Scene} the scene to add the lights to
 */
var addDirectionalFromConfig = function(scene) {
    lights = config.lights.directional;
    for(i in lights) {
        var light = new THREE.DirectionalLight(lights[i][1], lights[i][2]);
        light.position.set(lights[i][0][0], lights[i][0][1], lights[i][0][2]).normalize();
        scene.add(light);
    }
}

/**
 *
 *
 * @param {THREE.Scene} the scene to add the lights to
 */
var addAmbientFromConfig = function(scene) {
    scene.add(new THREE.AmbientLight(config.lights.ambient));
}

/**
 *
 *
 * @param {THREE.Scene} the scene to add the lights to
 */
var addSpotFromConfig = function(scene) {
    lights = config.lights.spot;
    for(i in lights) {
        var light = new THREE.SpotLight(lights[i][2], lights[i][3]);

        light.position.set(lights[i][0][0], lights[i][0][1], lights[i][0][2]);
        var temp = new THREE.Object3D(lights[i][1][0], lights[i][1][1], lights[i][1][2]);
        temp.position =  new THREE.Vector3(lights[i][1][0], lights[i][1][1], lights[i][1][2]);
        light.target = temp;

        //Shadow Stuff
        if(lights[i][4] == true) {
            light.castShadow = true;
            light.shadowCameraNear = .0001;
            light.shadowCameraFar = config.camera.far;
            light.shadowCameraFov = 90;
            light.shadowBias = 0.00000001;
            light.shadowDarkness = .6;
            light.shadowMapWidth = 1000;
            light.shadowMapHeight = 1000;
        }
        scene.add(light);
    }
}
