/*jshint globalstrict: true */
'use strict';

/**
 * @param {THREE.Scene} the scene the lights are associated with
 * @constructor
 */
function ChessLights(scene) {
    this.scene = scene;
    this.ambient = undefined;
    this.directional = [];
    this.spot = [];
}

ChessLights.prototype.setSpotLightIntensity = function(index, value) {
    this.getSpotLight(index).intensity = value;
}


ChessLights.prototype.numberSpotLights = function() {
    return this.spot.length;
}

ChessLights.prototype.getSpotLight = function(index) {
    return this.spot[index];
}

/**
 * Adds a spot light to the scene.
 *
 * @param {THREE.Vector3}
 * @param {THREE.Vector3}
 * @param {Hex} value of the color
 * @param {Number} between 0 and 1.0
 * @param {Boolean} whether or not to cast shadows
 * @param {Number} shadow camera near cutoff
 * @param {Number} shadow camera far cutoff
 * @param {Number} shadow camera feild of view
 * @param {Number} shadow bias (whatever that means)
 * @param {Number} how dark the shadow is (0 - 1.0)
 * @param {Number} shadow map width. Higher means sharper shadows but slower.
 * @param {Number} shadow map height. Higher means sharper shadows but slower.
 * @param {Boolean} whether or not the light only casts shadow
 */
ChessLights.prototype.addSpotLight = function(position, target, color, intensity, castShadow, shadowCameraNear, shadowCameraFar, shadowCameraFov, shadowBias, shadowDarkness, shadowMapWidth, shadowMapHeight, onlyShadow) {
    var light = new THREE.SpotLight(color, intensity);

    light.position.set(position.x, position.y, position.z);
    var temp = new THREE.Object3D();
    temp.position =  target;
    light.target = temp;

    //Shadow Stuff
    light.castShadow = castShadow == undefined ? false : castShadow;
    light.shadowCameraNear = shadowCameraNear == undefined ? .001 : shadowCameraNear;
    light.shadowCameraFar = shadowCameraFar == undefined ? config.camera.far : shadowCameraFar;
    light.shadowCameraFov = shadowCameraFov == undefined ? 90 : shadowCameraFov;
    light.shadowBias = shadowBias == undefined ? 0.00000001 : shadowBias;
    light.shadowDarkness = shadowDarkness == undefined ? .6 : shadowDarkness;
    light.shadowMapWidth = shadowMapWidth == undefined ? 1000 : shadowMapWidth;
    light.shadowMapHeight = shadowMapHeight == undefined ? 1000 : shadowMapHeight;
    light.onlyShadow = onlyShadow == undefined ? false : onlyShadow;
    

    this.spot.push(light);
    this.scene.add(light);
}

/**
 * Sets the ambient light of the scene
 *
 * @param {Hex} the color of the ambient light
 */
ChessLights.prototype.setAmbientLight = function(color) {
    this.ambient = new THREE.AmbientLight(color);
    this.scene.add(this.ambient);
}

/**
 * Adds a directional light to the scene.
 *
 * @param {THREE.Vector3}
 * @param {THREE.Vector3}
 * @param {Hex} value of the color
 * @param {Number} between 0 and 1.0
 */
ChessLights.prototype.addDirectionalLight = function(position, color, intensity) {
    var light = new THREE.DirectionalLight(color, intensity);
    light.position = position.normalize();
    this.directional.push(light);
    this.scene.add(light);
}

function ChessLightsFactoryFromConfig(scene) {
    var chessLights = new ChessLights(scene);

    chessLights.setAmbientLight(config.lights.ambient);

    var lights = config.lights.directional;
    for(var i in lights) {
        chessLights.addDirectionalLight(new THREE.Vector3(lights[i][0][0], lights[i][0][1], lights[i][0][2]), new THREE.Vector3(lights[i][1][0], lights[i][1][1], lights[i][1][2]), lights[i][2], lights[i][3], lights[i][4], lights[i][5], lights[i][6], lights[i][7], lights[i][8], lights[i][9], lights[i][10], lights[i][11], lights[i][12]);
    }

   var lights = config.lights.spot;
    for(var i in lights) {
        chessLights.addSpotLight(new THREE.Vector3(lights[i][0][0], lights[i][0][1], lights[i][0][2]), new THREE.Vector3(lights[i][1][0], lights[i][1][1], lights[i][1][2]), lights[i][2], lights[i][3], lights[i][4], lights[i][5], lights[i][6], lights[i][7], lights[i][8], lights[i][9], lights[i][10], lights[i][11], lights[i][12]);
    }

    return chessLights;
}