/**
 * myGUI
 * Dat GUI wrapper
 */
function myGui() {
    var datGui = new dat.GUI();

    /**
     * Wrapper around dat.GUI.add
     * @param {{}} x object containing stuff
     * @param {String} y field to get from x
     * @return {DatGui}
     */
    this._add = function(x, y) {
        return datGui.add(x, y);
    };
}

/**
 * Add to our gui by a key-value
 * @param {String} key to name in the GUI
 * @param {{}} value whatever you wish it to be!
 * @return {DatGui}
 */
myGui.prototype.add = function(key, value) {
    var obj = {};
    obj[key] = value;
    return this._add(obj, key);
};
