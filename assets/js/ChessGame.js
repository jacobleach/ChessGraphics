/*jshint globalstrict: true */
'use strict';

/**
 * Our Chess ChessGame
 * @constructor
 */
function ChessGame() {
    // Set ourselves to the global variable chessGame
    window.chessBoard = this;

    this.scene = new THREE.Scene();
    this.lights = ChessLightsFactoryFromConfig(this.scene);
    this.animationHandler = new AnimationHandler();
    this.renderer = new THREE.WebGLRenderer({antialias: true});
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.renderer.physicallyBasedShading = true;
    this.renderer.shadowMapEnabled = true;
    this.renderer.shadowMapType = THREE.PCFSoftShadowMap;
    document.body.appendChild(this.renderer.domElement);
}

/**
 * @type {ChessCamera}
 */
ChessGame.prototype.camera = ChessCameraFromConfigFactory();


/**
 * @type {THREE.Renderer}
 */
ChessGame.prototype.renderer = null;


/**
 * @type {THREE.Object3D}
 */
ChessGame.prototype.scene = null;


/**
 * @type {ChessBoard}
 */
ChessGame.prototype.chessBoard = null;


/**
 * Render loop
 *
 * REQUIRES chessBoard = this
 * so we do not lose context
 */
ChessGame.prototype.render = function() {
    requestAnimationFrame(chessBoard.render);
    window.chessBoard.animationHandler.render();
    window.chessBoard.renderer.render(window.chessBoard.scene, window.chessBoard.camera.camera);
};


/**
 * Load the necessary models, and run render()
 */
ChessGame.prototype.loadAndStartRendering = function() {
    var self = this;
    var boardLoader = new THREE.OBJMTLLoader();
    boardLoader.load(
        config.models.boardObject,
        config.models.boardMaterial,
        function(object) {
            ChessBoard.prototype.model = object;
        }

    )

    var materialLoader = new THREE.MTLLoader(config.models.materialsDirectory);
    materialLoader.load(
        config.models.darkMaterial,
        function(object) {
            ChessPiece.prototype.blackMaterial = object.create( "Material" );
            materialLoader.load(
                config.models.lightMaterial,
                function(object) {
                    ChessPiece.prototype.whiteMaterial = object.create( "Material" );
                }
            );
        }
    );

    var loader = new THREE.OBJLoader();
        // Pawn
        loader.load(
            config.models.pawnObject,
            function(object) {
                Pawn.prototype.toCopyModel = object;
                // King
                loader.load(
                    config.models.kingObject,
                    function(object) {
                        King.prototype.toCopyModel = object;
                        // Queen
                        loader.load(
                            config.models.queenObject,
                            function(object) {
                                Queen.prototype.toCopyModel = object;
                                // Rook
                                loader.load(
                                    config.models.rookObject,
                                    function(object) {
                                        Rook.prototype.toCopyModel = object;
                                        // Knight
                                        loader.load(
                                            config.models.knightObject,
                                            function(object) {
                                                Knight.prototype.toCopyModel = object;
                                                // Bishop
                                                loader.load(
                                                    config.models.bishopObject,
                                                    function(object) {
                                                        Bishop.prototype.toCopyModel = object;

                                                        self.chessBoard = new ChessBoard();
                                                        self.scene.add(self.chessBoard.model);
                                                        self.render();
                                                    }
                                                );
                                            }
                                        );
                                    }
                                );
                            }
                        );
                    }
                );
            }
        );
}
