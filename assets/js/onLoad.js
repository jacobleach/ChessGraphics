/*jshint globalstrict: true */
'use strict';
var chessGame;
var modelSet = 1;
/**
 * Happens on onload
 */
window.onload = function() {
  chessGame = new ChessGame();
  chessGame.loadAndStartRendering();

  var gui = new myGui();
  gui.add("BoardTexture", 0)
  .step(1)
  .min(0)
  .max(15)
  .onChange(
          function(value) {
              setBoardMaterial(config.models.materialsDirectory + "board" + Math.floor(value) + ".mtl");
          }
  );

  gui.add("WhiteTexture", 0)
  .step(34)
  .min(0)
  .max(127)
  .onChange(
          function(value) {
              setWhiteMaterial(config.models.materialsDirectory + "white" + Math.floor(value) + ".mtl");
          }
  );

  gui.add("BlackTexture", 0)
  .step(34)
  .min(0)
  .max(127)
  .onChange(
          function(value) {
              setBlackMaterial(config.models.materialsDirectory + "black" + Math.floor(value) + ".mtl");
          }
);

gui.add("Switch Models",
    function() {
        switch (modelSet) {
            case 0:
                loadModels('assets/models/current/');
                modelSet = 1;
                break;
            case 1:
                loadModels('assets/models/old/');
                modelSet = 0;
                break;
            default:
                loadModels('assets/models/current/');
                modelSet = 1;
                break;
        }
    }
);

  //I could not get callbacks to work with a for loop
  //So I had to hard code in the lights
  //I wanted them to be dynamic and they were
  //Except I couldn't change the values correctly :(

  gui.add("Light " + 1 + " Intensity", window.chessGame.lights.getSpotLight(0).intensity).step(.01).min(0).max(5).onChange(function(value) { 
    window.chessGame.lights.setSpotLightIntensity(0, value);
  });

  gui.add("Light " + 2 + " Intensity", window.chessGame.lights.getSpotLight(0).intensity).step(.01).min(0).max(5).onChange(function(value) { 
    window.chessGame.lights.setSpotLightIntensity(1, value);
  });

  gui.add("Light " + 3 + " Intensity", window.chessGame.lights.getSpotLight(0).intensity).step(.01).min(0).max(5).onChange(function(value) { 
    window.chessGame.lights.setSpotLightIntensity(2, value);
  });

  gui.add("Light " + 4 + " Intensity", window.chessGame.lights.getSpotLight(0).intensity).step(.01).min(0).max(5).onChange(function(value) { 
    window.chessGame.lights.setSpotLightIntensity(3, value);
  });

  gui.add("Sun Intensity", window.chessGame.lights.directional[0].intensity).step(.01).min(0).max(5).onChange(function(value) { 
   window.chessGame.lights.directional[0].intensity = value;
  });


  gui.add(
      "Play",
      function () {
          var gameURL = window.prompt("What is the URL of the game you would like to play?");


          window.chessGame.scene.remove(window.chessGame.chessBoard.model);
          window.chessGame.chessBoard = new ChessBoard();
          window.chessGame.scene.add(window.chessGame.chessBoard.model);

          // wait 1 second for the secene to reload
          setTimeout(function () { play(gameURL) }, 1000);;

          // countdown timer
          window.countdownTimer = setInterval(
              function () {
                  var gameJSON = getGameJSON();

                  if (gameJSON.gameover) {
                      clearInterval(window.countdownTimer);
                  }
                  var whiteClock = Math.floor(parseFloat(gameJSON.whitetime));
                  var blackClock = Math.floor(parseFloat(gameJSON.blacktime));
                  window.text2.innerHTML = "<br>White Time: " + whiteClock + "<br>Black Time: " + blackClock;
              },
              1000
          );
      }
  );

  gui.add(
      "Stop",
      function () { location.reload(); }
  );

    // http://stackoverflow.com/questions/15248872/dynamically-create-2d-text-in-three-js
    // clock
    window.text2 = document.createElement('div');
    text2.style.position = 'absolute';
    //text2.style.zIndex = 1;    // if you still don't see the label, try uncommenting this
    // text2.style.width = 100;
    // text2.style.height = 100;
    text2.style.backgroundColor = "orange";
    text2.innerHTML = "<br>White Time: 0 <br>Black Time: 0";
    text2.style.top = '1%';
    text2.style.left = '50%';
    document.body.appendChild(text2);

    // game board pt 2
    window.text3 = document.createElement('div');
    text3.style.position = 'absolute';
    //text2.style.zIndex = 1;    // if you still don't see the label, try uncommenting this
    // text3.style.width = 100;
    // text3.style.height = 100;
    text3.style.backgroundColor = "orange";
    // text3.innerHTML = chessGame.chessBoard.toString();
    text3.style.top = '1%';
    text3.style.left = '1%';
    text3.style.fontFamily = 'LucidaConsole,monospace';
    text3.style.fontSize = '300%';
    document.body.appendChild(text3);

    // black team name
    window.text4 = document.createElement('div');
    text4.style.position = 'absolute';
    //text2.style.zIndex = 1;    // if you still don't see the label, try uncommenting this
    // text3.style.width = 100;
    // text3.style.height = 100;
    text4.style.backgroundColor = "orange";
    // text3.innerHTML = chessGame.chessBoard.toString();
    text4.style.top = '15%';
    text4.style.left = '50%';
    text4.style.fontFamily = 'LucidaConsole,monospace';
    text4.style.fontSize = '100%';
    document.body.appendChild(text4);

    // white team name
    window.text5 = document.createElement('div');
    text5.style.position = 'absolute';
    //text2.style.zIndex = 1;    // if you still don't see the label, try uncommenting this
    // text3.style.width = 100;
    // text3.style.height = 100;
    text5.style.backgroundColor = "orange";
    // text3.innerHTML = chessGame.chessBoard.toString();
    text5.style.bottom = '3%';
    text5.style.left = '50%';
    text5.style.fontFamily = 'LucidaConsole,monospace';
    text5.style.fontSize = '100%';
    document.body.appendChild(text5);
};


