/*jshint globalstrict: true */
'use strict';

/**
 * @author travisby@gmai.com (Travis Beatty)
 */


/**
 * Determines whether every item in list conforms to the test func
 * @param {function(Object)} func
 * @param {Array} list
 * @return {boolean}
 */
function all(func, list) {
  for (var i = 0; i < list.length; i++) {
    if (!func(list[i])) {
      return false;
    }
  }
  return true;
}


/**
 * Determines whether any item in list conforms to the test func
 * @param {function(Object)} func
 * @param {Array} list
 * @return {boolean}
 */
function any(func, list) {
  for (var i = 0; i < list.length; i++) {
    if (func(list[i])) {
      return true;
    }
  }
  return false;
}


/**
 * Determines wether a character is an alpha-character
 * @param {char} character
 * @return {boolean}
 */
function isAlpha(character) {
  var charCode = character.charCodeAt(0);

  // A value can be a character iff its array length is one
  if (character.length !== 1) {
    return false;
  }

  return (charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123);
}


/**
 * Determines whether every character in str is an alpha-character
 * @param {string} str
 * @return {boolean}
 */
function isAlphas(str) {
  return all(isAlpha, str);
}


/**
 * Determines if item is an integer
 * @param {Object} item
 * @return {boolean}
 */
function isInt(item) {
  // http://stackoverflow.com/a/4515694
  if (typeof item === 'number' && item % 1 === 0) {
    return true;
  }
  return false;
}


/**
 *
 * @param {String} gameURL to access
 * @returns {Object} Game JSON
 */
function getGameJSON(gameURL, test) {
    if (!gameURL) {
        return JSON.parse('{"whitename": "white", "blackname": "bitchin", "lastmovenumber": 87, "blacktime": 1108.887688, "gameover": true, "whitesturn": false, "moves": ["Pe2e4", "Pd7d6", "Pd2d4", "Ng8f6", "Nb1c3", "Pg7g6", "Bc1e3", "Bf8g7", "Qd1d2", "Pc7c6", "Pf2f3", "Pb7b5", "Ng1e2", "Nb8d7", "Be3h6", "Bg7h6", "Qd2h6", "Bc8b7", "Pa2a3", "Pe7e5", "Ke1c1", "Qd8e7", "Kc1b1", "Pa7a6", "Ne2c1", "Ke8c8", "Nc1b3", "Pe5d4", "Rd1d4", "Pc6c5", "Rd4d1", "Nd7b6", "Pg2g3", "Kc8b8", "Nb3a5", "Bb7a8", "Bf1h3", "Pd6d5", "Qh6f4", "Kb8a7", "Rh1e1", "Pd5d4", "Nc3d5", "Nb6d5", "Pe4d5", "Qe7d6", "Rd1d4", "Pc5d4", "Re1e7", "Ka7b6", "Qf4d4", "Kb6a5", "Pb2b4", "Ka5a4", "Qd4c3", "Qd6d5", "Re7a7", "Ba8b7", "Ra7b7", "Qd5c4", "Qc3f6", "Ka4a3", "Qf6a6", "Ka3b4", "Pc2c3", "Kb4c3", "Qa6a1", "Kc3d2", "Qa1b2", "Kd2d1", "Bh3f1", "Rd8d2", "Rb7d7", "Rd2d7", "Bf1c4", "Pb5c4", "Qb2h8", "Rd7d3", "Qh8a8", "Pc4c3", "Qa8a4", "Kd1e1", "Pf3f4", "Pf7f5", "Kb1c1", "Rd3d2", "Qa4a7"], "whitetime": 920.588292}');
    }
    return JSON.parse($.ajax({
        type: "GET",
        url: gameURL,
        cache: false,
        async: false
    }).responseText);
}


/**
 * Builds a chess position from a string
 * @param {String} str of move "<letter><number>" each one character
 * @returns {ChessPosition} that str represented
 */
function stringToChessPosition(str) {
    return new ChessPosition(str[0], parseInt(str[1], 10));
}


/**
 * Takes in str of move and moves the piece
 * @param str in form "<letter><letter><number><letter><number>" where the first letter is ignored, and the rest are coordinates
 */
function performMoveFromString(str) {
    var promotePawnTo = null;
    var startPosition = stringToChessPosition(str.slice(1, 3));
    var endPosition = stringToChessPosition(str.slice(3, 5));
    // does this involve pawn promotion?
    if (str.length > 5) {
        promotePawnTo = str[5];
    }
    window.chessBoard.chessBoard.moveAToB(startPosition, endPosition, promotePawnTo);
}

/**
 * Plays through an entire game
 * @param {String} gameURL to get the game from
 */
function play(gameURL) {
    var game = getGameJSON(gameURL);

    // update player names
    window.text4.innerHTML = game.blackname;
    window.text5.innerHTML = game.whitename;

    /**
     * Recursive function to run through all the moves
     * @param {[]} moves array
     */
    var game_recur = function(moves, lastmove) {

        // poll for everyone to stop moving
        if (window.pieceIsMoving || !game.moves) {
            window.gameHandle = setTimeout(function () { game_recur([], lastmove);}, 500);
            console.log("waiting...")
            return;
        }
        console.log("last move..." + lastmove);
        var newGame;
        var move = moves.shift();
        var temp;

        // base case
        if (!move) {
            newGame = getGameJSON(gameURL);
            moves = newGame.moves;
            if (newGame.gameover && parseInt(newGame.lastmovenumber, 10) === lastmove - 1) {
                gameOver(newGame.whitesturn);
                return;
            }
            // else implied
            // remove the moves we have already performed
            temp = 1;
            while (temp < lastmove) {
                moves.shift();
                temp++;
            }
            // the game is not over, but we still have no new moves
            if (!moves.length) {
                console.log("Polling for moves...");
                window.gameHandle = setTimeout(function () { game_recur([], lastmove);}, 500);
                return;
            }
            move = moves.shift();

        }

        // do work
        performMoveFromString(move);

        // recursive step
        window.gameHandle = setTimeout(function () { game_recur(moves, ++lastmove);}, 1);
    };

    game_recur(game.moves, 1);
}

/**
 * Handles game over functionallity
 */
function gameOver(whitesturn) {
   if (!whitesturn) {
       alert("White wins!");
   } else {
       alert("Black wins!");
   }
}
function nl2br (str, is_xhtml) {
  // http://kevin.vanzonneveld.net
  // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +   improved by: Philip Peterson
  // +   improved by: Onno Marsman
  // +   improved by: Atli Þór
  // +   bugfixed by: Onno Marsman
  // +      input by: Brett Zamir (http://brett-zamir.me)
  // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +   improved by: Brett Zamir (http://brett-zamir.me)
  // +   improved by: Maximusya
  // *     example 1: nl2br('Kevin\nvan\nZonneveld');
  // *     returns 1: 'Kevin<br />\nvan<br />\nZonneveld'
  // *     example 2: nl2br("\nOne\nTwo\n\nThree\n", false);
  // *     returns 2: '<br>\nOne<br>\nTwo<br>\n<br>\nThree<br>\n'
  // *     example 3: nl2br("\nOne\nTwo\n\nThree\n", true);
  // *     returns 3: '<br />\nOne<br />\nTwo<br />\n<br />\nThree<br />\n'
  var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br ' + '/>' : '<br>'; // Adjust comment to avoid issue on phpjs.org display

  return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}
