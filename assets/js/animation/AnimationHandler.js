/*jshint globalstrict: true */
'use strict';



/**
 * @author leach.jacob@gmail.com (Jacob Leach)
 * @constructor
 */
function AnimationHandler() {
  this.animations = [];
}


/**
 * Adds animation
 * @param {Animation} animation to run
 */
AnimationHandler.prototype.add = function(animation) {
  this.animations.push(animation);
};


/**
 * Draws
 */
AnimationHandler.prototype.render = function() {
  for (var i in this.animations) {
    if (this.animations[i].animate() == true) {
      this.animations.splice(i, 1);
    }
  }
};
