/*jshint globalstrict: true */
'use strict';

/**
 * @author leach.jacob@gmail.com (Jacob Leach)
 * @extends {Animation}
 * @param {Object3D} object3D
 * @param {Vector3} destination
 * @constructor
 */
function RiseAndFall(object3D, destination) {
    Animation.apply(this, [object3D, destination]);
    pieceIsMoving = true;

    this.startLocation = this.piece.getLocation();

    this.distX = Math.abs(this.piece.getLocation().x - this.destination.x);
    this.distZ = Math.abs(this.piece.getLocation().z - this.destination.z);

    this.incrX = this.distX / 60;
    this.incrZ = this.distZ / 60;
    this.slideDistance = 0;
    this.point = 0;
    window.pieceIsMoving = true;
}

RiseAndFall.prototype = Object.create(Animation.prototype);


/**
 * Our constructor
 */
RiseAndFall.prototype.constructor = RiseAndFall;


/**
 * Draws our slide
 */
RiseAndFall.prototype.animate = function() {
    if(this.point == 0) {
        if(this.slideDistance >= 2) {
            this.piece.translate(new THREE.Vector3(this.piece.getLocation().x, 2, this.piece.getLocation().z));
            this.point = 1;
        }
        else {

            this.slideDistance += .05;
            this.piece.translate(new THREE.Vector3(this.piece.getLocation().x, this.destination.y + this.slideDistance, this.piece.getLocation().z));
        }
    }
    else if(this.point == 1) {
        var x = this.piece.getLocation().x;
        var z = this.piece.getLocation().z;

        if(this.distX > 0) {

            if(x < this.destination.x) {
                x += this.incrX;
            }
            else {
                x -= this.incrX;
            }

            this.distX -= this.incrX;
        }

        if(this.distZ > 0) {
            if(z < this.destination.z) {
                z += this.incrZ;
            }
            else {
                z -= this.incrZ;
            }

            this.distZ -= this.incrZ;
        }

        var y = this.piece.getLocation().y;

        var temp = new THREE.Vector3(x, y, z);
        this.piece.translate(temp);

        if(this.distX <= 0 && this.distZ <= 0)
        {
            this.point = 2;
        }

    }
    else {
        if(this.slideDistance <= 0) {
            this.piece.translate(this.destination);
            window.pieceIsMoving = false;
            return true;
        }
        else {
            this.slideDistance -= .05;
            this.piece.translate(new THREE.Vector3(this.destination.x, this.destination.y + this.slideDistance, this.destination.z));
        }
    }


};
