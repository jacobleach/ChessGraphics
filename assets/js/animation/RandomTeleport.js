/*jshint globalstrict: true */
'use strict';

/**
 * @author leach.jacob@gmail.com (Jacob Leach)
 * @extends {Animation}
 * @param {Object3D} object3D
 * @param {Vector3} destination
 * @constructor
 */
function RandomTeleport(object3D, destination) {
    Animation.apply(this, [object3D, destination]);
    this.waitCount = 0;
    this.teleportCount = 0;
    this.slide = false;
    this.slideDistance = 1;
    this.done = false;
}

RandomTeleport.prototype = Object.create(Animation.prototype);


/**
 * Our constructor
 */
RandomTeleport.prototype.constructor = RandomTeleport;


/**
 * Draws our teleport
 */
RandomTeleport.prototype.animate = function() {
    if(this.done) {
        if(!this.slide) {
            if(this.waitCount <= 15) {
                this.piece.translate(new THREE.Vector3(this.destination.x, this.destination.y + 1, this.destination.z));
                this.waitCount++;
            }
            else {
                this.slide = true;
            }
        }
        else {
            if(this.slideDistance <= 0) {
                this.piece.translate(this.destination);
                return true;
            }
            else {
                this.slideDistance -= .05;
                this.piece.translate(new THREE.Vector3(this.destination.x, this.destination.y + this.slideDistance, this.destination.z));
            }
        }
    }
    else if(this.waitCount == 15 && !this.done) {
        this.teleportCount++;

        if(this.teleportCount == 4) {
            this.done = true;
        }
        else {
            this.piece.translate(getRandomPlace());
        }

        this.waitCount = 0;
    }
    else {
        this.waitCount++;
    }

    return false;
};

function getRandomPlace() {
    return new THREE.Vector3(Math.floor((Math.random() * 8) - 4), 2, Math.floor((Math.random() * 8) - 4));
}
