/*jshint globalstrict: true */
'use strict';

/**
 * @author leach.jacob@gmail.com (Jacob Leach)
 * @extends {Animation}
 * @param {Object3D} object3D
 * @param {Vector3} destination
 * @constructor
 */
function Slide(object3D, destination) {
    Animation.apply(this, [object3D, destination]);

    this.distX = Math.abs(this.piece.getLocation().x - this.destination.x);
    this.distZ = Math.abs(this.piece.getLocation().z - this.destination.z);

    this.incrX = this.distX / 60;
    this.incrZ = this.distZ / 60;
    console.log(this.incrX);
    console.log(this.incrZ);
}

Slide.prototype = Object.create(Animation.prototype);


/**
 * Our constructor
 */
Slide.prototype.constructor = Slide;


/**
 * Draws our slide
 */
Slide.prototype.animate = function() {
     var x = this.piece.getLocation().x;
     var z = this.piece.getLocation().z;
     if(this.distX > 0) {
        if(x < this.destination.x) {
            x += this.incrX;
        }
        else {
            x -= this.incrX;
        }
        this.distX -= this.incrX;
    }

    if(this.distZ > 0) {
        if(z < this.destination.z) {
            z += this.incrZ;
        }
        else {
            z -= this.incrZ;
        }
        this.distZ -= this.incrZ;
    }

    var y = this.piece.getLocation().y;

    var temp = new THREE.Vector3(x, y, z);
    this.piece.translate(temp);
    if(this.distX <= 0 && this.distZ <= 0)
    {
        console.log(this.distX);
        return true;
    }

    return false;
};
