/*jshint globalstrict: true */
'use strict';

/**
 * @param {ChessPeice} piece individual chess piece to animate
 * @param {Vector3} destination to translate to
 * @constructor
 */
function Animation(piece, destination) {
  this.piece = piece;
  this.destination = destination;
}


/**
 * Draws our Animation
 */
Animation.prototype.animate = function() {};

/**
 * @param {ChessPeice} piece individual chess piece to animate
 * @param {Vector3} destination to translate to
 * @returns {Animation} a random movement animation
 */
function RandomMovementAnimation(piece, destination) {
	var number = Math.floor((Math.random() * 100));

	if(number < 50) {
		return new Slide(piece, destination);
	}
	else if(number > 50 && number < 90) {
		return new RandomTeleport(piece, destination);
	}
	else {
		return new Teleport(piece, destination);
	}
}
