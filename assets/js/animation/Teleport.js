/*jshint globalstrict: true */
'use strict';



/**
 * @author leach.jacob@gmail.com (Jacob Leach)
 * @extends {Animation}
 * @param {Object3D} object3D
 * @param {Vector3} destination
 * @constructor
 */
function Teleport(object3D, destination) {
  Animation.apply(this, [object3D, destination]);
}

Teleport.prototype = Object.create(Animation.prototype);


/**
 * Our constructor
 */
Teleport.prototype.constructor = Teleport;


/**
 * Draws our teleport
 */
Teleport.prototype.animate = function() {
    this.piece.translate(this.destination);
    return true;
};


