/*jshint globalstrict: true */
'use strict';

/**
 * @author leach.jacob@gmail.com (Jacob Leach)
 * @extends {Animation}
 * @param {Object3D} object3D
 * @param {Vector3} destination
 * @constructor
 */
function KnockOut(object3D, amount) {
  Animation.apply(this, [object3D, undefined]);
  this.incr = (amount > 0) ? -.05 : .05;
  this.amount = Math.abs(amount);
}

KnockOut.prototype = Object.create(Animation.prototype);


/**
 * Our constructor
 */
KnockOut.prototype.constructor = Teleport;


/**
 * Draws our teleport
 */
KnockOut.prototype.animate = function() {
  if(this.amount >= 0) {
    	this.piece.model.translateX(Math.abs(this.incr));
      this.piece.model.translateY(Math.abs(this.incr) * 2);

    	this.amount -= Math.abs(this.incr);

    	return false;
	}

	return true;
};
