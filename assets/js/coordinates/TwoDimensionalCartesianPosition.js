/*jshint globalstrict: true */
'use strict';

/**
 * @author travisby@gmail.com (Travis Beatty)
 * @implements {Position}
 */

function TwoDimensionalCartesianPosition() {
}


/**
 * Gets how far over a position is
 * @type {Object}
 */
TwoDimensionalCartesianPosition.prototype.getOver = function() {
};


/**
 * Gets how far up a position is
 * @type {Object}
 */
TwoDimensionalCartesianPosition.getUp = function() {};
