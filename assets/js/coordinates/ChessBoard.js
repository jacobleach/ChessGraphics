/*jshint globalstrict: true */
'use strict';

/**
 * @author travisby@gmail.com (Travis Beatty)
 */



/**
 * Space for playing Chess
 * @constructor
 * @implements {Space}
 * @param {Object3D} the chess board's model
 */
function ChessBoard() {
    this.model = this.model.clone();

    this.model.traverse(
        function(mesh) {
            if(mesh instanceof THREE.Mesh) {
                mesh.receiveShadow = true;
            }
        }
    );

    // load all the pieces
    // black
    this.coordinates['A8'] = new Rook(new ChessPosition('a', 8), BLACK_MATERIAL);
    this.coordinates['B8'] = new Knight(new ChessPosition('b', 8), BLACK_MATERIAL);
    this.coordinates['C8'] = new Bishop(new ChessPosition('c', 8), BLACK_MATERIAL);
    this.coordinates['D8'] = new Queen(new ChessPosition('d', 8), BLACK_MATERIAL);
    this.coordinates['E8'] = new King(new ChessPosition('e', 8), BLACK_MATERIAL);
    this.coordinates['F8'] = new Bishop(new ChessPosition('f', 8), BLACK_MATERIAL);
    this.coordinates['G8'] = new Knight(new ChessPosition('g', 8), BLACK_MATERIAL);
    this.coordinates['H8'] = new Rook(new ChessPosition('h', 8), BLACK_MATERIAL);
    this.coordinates['A7'] = new Pawn(new ChessPosition('a', 7), BLACK_MATERIAL);
    this.coordinates['B7'] = new Pawn(new ChessPosition('b', 7), BLACK_MATERIAL);
    this.coordinates['C7'] = new Pawn(new ChessPosition('c', 7), BLACK_MATERIAL);
    this.coordinates['D7'] = new Pawn(new ChessPosition('d', 7), BLACK_MATERIAL);
    this.coordinates['E7'] = new Pawn(new ChessPosition('e', 7), BLACK_MATERIAL);
    this.coordinates['F7'] = new Pawn(new ChessPosition('f', 7), BLACK_MATERIAL);
    this.coordinates['G7'] = new Pawn(new ChessPosition('g', 7), BLACK_MATERIAL);
    this.coordinates['H7'] = new Pawn(new ChessPosition('h', 7), BLACK_MATERIAL);

    // white
    this.coordinates['A1'] = new Rook(new ChessPosition('a', 1), WHITE_MATERIAL);
    this.coordinates['B1'] = new Knight(new ChessPosition('b', 1), WHITE_MATERIAL);
    this.coordinates['C1'] = new Bishop(new ChessPosition('c', 1), WHITE_MATERIAL);
    this.coordinates['D1'] = new Queen(new ChessPosition('d', 1), WHITE_MATERIAL);
    this.coordinates['E1'] = new King(new ChessPosition('e', 1), WHITE_MATERIAL);
    this.coordinates['F1'] = new Bishop(new ChessPosition('f', 1), WHITE_MATERIAL);
    this.coordinates['G1'] = new Knight(new ChessPosition('g', 1), WHITE_MATERIAL);
    this.coordinates['H1'] = new Rook(new ChessPosition('h', 1), WHITE_MATERIAL);
    this.coordinates['A2'] = new Pawn(new ChessPosition('a', 2), WHITE_MATERIAL);
    this.coordinates['B2'] = new Pawn(new ChessPosition('b', 2), WHITE_MATERIAL);
    this.coordinates['C2'] = new Pawn(new ChessPosition('c', 2), WHITE_MATERIAL);
    this.coordinates['D2'] = new Pawn(new ChessPosition('d', 2), WHITE_MATERIAL);
    this.coordinates['E2'] = new Pawn(new ChessPosition('e', 2), WHITE_MATERIAL);
    this.coordinates['F2'] = new Pawn(new ChessPosition('f', 2), WHITE_MATERIAL);
    this.coordinates['G2'] = new Pawn(new ChessPosition('g', 2), WHITE_MATERIAL);
    this.coordinates['H2'] = new Pawn(new ChessPosition('h', 2), WHITE_MATERIAL);

    // add them to our board
    for (var i in this.coordinates) {
        this.model.add(this.coordinates[i].model);
    }

    // update the cool cli version
    window.text3.innerHTML = nl2br(this.toString());

};

ChessBoard.prototype.reload = function() {
    for(var i in this.coordinates) {
        if(this.coordinates[i] != undefined) {

            this.model.remove(this.coordinates[i].model);
       
            if(this.coordinates[i] instanceof Pawn) {
                 this.coordinates[i] = new Pawn(this.coordinates[i].position, this.coordinates[i].isBlack() ? BLACK_MATERIAL : WHITE_MATERIAL);
            }
            else if(this.coordinates[i] instanceof King) {
                 this.coordinates[i] = new King(this.coordinates[i].position, this.coordinates[i].isBlack() ? BLACK_MATERIAL : WHITE_MATERIAL);
            }
            else if(this.coordinates[i] instanceof Queen) {
                 this.coordinates[i] = new Queen(this.coordinates[i].position, this.coordinates[i].isBlack() ? BLACK_MATERIAL : WHITE_MATERIAL);
            }
            else if(this.coordinates[i] instanceof Rook) {
                 this.coordinates[i] = new Rook(this.coordinates[i].position, this.coordinates[i].isBlack() ? BLACK_MATERIAL : WHITE_MATERIAL);
            }
            else if(this.coordinates[i] instanceof Knight) {
                 this.coordinates[i] = new Knight(this.coordinates[i].position, this.coordinates[i].isBlack() ? BLACK_MATERIAL : WHITE_MATERIAL);
            }
            else if(this.coordinates[i] instanceof Bishop) {
                 this.coordinates[i] = new Bishop(this.coordinates[i].position, this.coordinates[i].isBlack() ? BLACK_MATERIAL : WHITE_MATERIAL);
            }
            else {
                console.log("Yo, jackasses! Your code is broken!");
            }

            this.model.add(this.coordinates[i].model);
        }
    }
}

/**
 * Updates all of the object's materials.
 */
ChessBoard.prototype.updateAll = function() {
    for (var i in this.coordinates) {
        if(this.coordinates[i] != undefined) {
            this.coordinates[i].updateMaterials();
        }
    }
}

ChessBoard.prototype.updateBoard = function() {
    if(this.model != ChessBoard.prototype.model) {
        var temp = this.model;
        this.model = ChessBoard.prototype.model;

        //Bit of a hack to make shadows keep working
        this.model.traverse(
            function(mesh) {
                if(mesh instanceof THREE.Mesh) {
                    mesh.receiveShadow = true;
                }
            }
        );

        for (var i in this.coordinates) {
            if(this.coordinates[i] != undefined) {
                this.model.add(this.coordinates[i].model);
            }
        }
        window.chessBoard.scene.remove(temp);
        window.chessBoard.scene.add(this.model);
    }
}

/**
 * TODO:
 */
ChessBoard.prototype.material = null;

ChessBoard.prototype.get3DObjects = function() {
    // TODO return everything
    return [this.model];
};

ChessBoard.prototype.model = null;

ChessBoard.prototype.coordinates = {
};

/**
 * @param {ChessPosition} a
 * @param {ChessPosition} b
 * @param {Character} c what a pawn should be promoted ot
 */
ChessBoard.prototype.moveAToB = function(a, b, c) {
    var pieceAtA = this.coordinates[a.toString()];
    var pieceAtB = this.coordinates[b.toString()];
    var rooksPositionForCastling;
    var rookForCastling;
    var newRooksPosition;
    var positionOfPawnInEnPassant;
    var colorForPromotion;

    /*
     * Different cases that might arise (in order of detection):
     * Castling
     * En Passant
     * Pawn Promotion
     * Capturing
     * Normal Movement
     *
     * Order matters for... (en passant && pawn promotion) -> capturing.  Both en passant and pawn promotion may set
     * pieceAtB, which is then used in capturing.
     */

    // castling
    // we check if the piece is a King, and it is moving > 1 space
    if (King.prototype.isPrototypeOf(pieceAtA) && Math.abs(a.getDisplacementX(b)) > 1) {
        // determine if we are castling left or right
        // if b.x == 'G', then we are going to the right
        if (b.getX() === 'G') {
            // the rook is in H<1 or 8>
            rooksPositionForCastling = new ChessPosition('h', a.getY());
            rookForCastling = this.coordinates[rooksPositionForCastling.toString()];
            newRooksPosition = new ChessPosition('f', a.getY());
        }
        // else we are going to the left
        else {
            // the rook is in A<1 or 8>
            rooksPositionForCastling = new ChessPosition('a', a.getY());
            rookForCastling = this.coordinates[rooksPositionForCastling.toString()];
            newRooksPosition = new ChessPosition('d', a.getY());

        }
        this.coordinates[rooksPositionForCastling] = undefined;
        rookForCastling.slideTo(newRooksPosition);
        this.coordinates[newRooksPosition] = rookForCastling;

    }

    // en passant
    // we detect this by deciding if a pawn has moved in X, there was a piece in the (b.x, b.y - 1) position,
    // AND we did not plan on capturing any pieces.  We cannot move laterally if we are not capturing.  If there was no
    // traditional capture detection, it must be en passant
    if (
        Pawn.prototype.isPrototypeOf(pieceAtA) &&
            a.getDisplacementX(b) &&
            this.coordinates[new ChessPosition(b.getX(), parseInt(b.getY(), 10) - 1)] &&
            !pieceAtB
    ) {
        positionOfPawnInEnPassant = new ChessPosition(b.getX(), parseInt(b.getY(), 10) - 1);
        pieceAtB = this.coordinates[positionOfPawnInEnPassant];
    }

    // pawn promotion
    // we detect this if we c is true.  If we were passed c, then we have a type to change to.
    if (c) {
        // TODO
        // this is not working.  For some reason the model knows it is at C2, however it's movement travels much farther.
        // over to the right and up three more than it should (for c2 -> c3, 'B')

        // remove the pawn~ish looking model
        colorForPromotion = pieceAtA.color;
        this.model.remove(pieceAtA.model);

        // choose our new model..
        switch (c) {
            case 'R':
                pieceAtA = new Rook(a, colorForPromotion);
                break;
            case 'N':
                pieceAtA = new Knight(a, colorForPromotion);
                break;
            case 'B':
                pieceAtA = new Bishop(a, colorForPromotion);
                break;
            case 'Q':
                pieceAtA = new Queen(a, colorForPromotion);
                break;
            // we are to assume every move is valid, right?
            case 'K':
                pieceAtA = new King(a, colorForPromotion);
                break;
            case 'P':
                pieceAtA = new Pawn(a, colorForPromotion);
                break;
            default:
                pieceAtA = new Pawn(a, colorForPromotion);
        }
        // and add it...

        this.model.add(pieceAtA.model);
        // in its old place
        this.coordinates[a] = pieceAtA;
    }

    // capturing
    if (pieceAtB) {
        window.chessBoard.animationHandler.add(new KnockOut(this.coordinates[b], 20000));
       //this.model.remove(pieceAtB.model);
    }

    // regular movement
    pieceAtA.animateTo(b);
    this.coordinates[b] = pieceAtA;
    delete this.coordinates[a];

    // update the cool cli version
    window.text3.innerHTML = nl2br(window.chessGame.chessBoard.toString());

};


ChessBoard.prototype.toString = function() {
    var letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];
    var numbers = [8, 7, 6, 5, 4, 3, 2, 1];
    var str = '';

    for (var y in numbers) {
        str = str.concat('\n');
        for (var x in letters) {
            console.log(letters[x] + numbers[y]);
            var lineStr = ''
            var piece = this.coordinates[letters[x] + numbers[y]];

            if (piece) {
                str = str.concat(piece.getChar());
            } else {
                str = str.concat('-');
            }
        }
    }
    return str.slice(1, str.length);;
}
