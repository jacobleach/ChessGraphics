/*jshint globalstrict: true */
'use strict';

/**
 * @author travisby@gmail.com (Travis Beatty)
 */



/**
 * Point on a {ChessBoard}
 * @param {string} x letter of position in the X direction
 * @param {number} y number of position in Y direction
 * @constructor
 */
function ChessPosition(x, y) {
  // validate input
  if (!isAlpha(x)) {
    x = null;
  }
  if (!isInt(y)) {
    y = null;
  }

  x = x.toUpperCase() || 'A';
  y = y || 0;

  /**
     * Gets how far over a position is
     * @return {string}
     */
  this.getX = function() {
    return x;
  };

  /**
     * Gets how far up a position is
     * @return {number}
     */
  this.getY = function() {
    return y;
  };
}
ChessPosition.prototype = new TwoDimensionalCartesianPosition();



/**
 * @constructor
 */
ChessPosition.prototype.constructor = ChessPosition;


// Implemented methods by another name
/**
 * Get how many units over we are
 */
ChessPosition.prototype.getOver = ChessPosition.prototype.getX;


/**
 * Get how many units up/down we are
 */
ChessPosition.getUp = ChessPosition.prototype.getY;

/**
 * Returns how far away in the X direction another position is
 * @type {ChessPosition}
 */
ChessPosition.prototype.getDisplacementX = function (newPosition) {
    return - (this.getX().charCodeAt(0) - newPosition.getX().charCodeAt(0));
};

/**
 * Returns how far away in the Y direction another position is
 * @type {ChessPosition}
 */
ChessPosition.prototype.getDisplacementY = function(newPosition) {
    // return newPosition.getY() - this.getY();
    return this.getY() - newPosition.getY();
};

/**
 * String representation of our object
 * @return {String}
 */
ChessPosition.prototype.toString = function() {
    return "" + this.getX() + this.getY();
};
