/*jshint globalstrict: true */
'use strict';

/**
 * @author travisby@gmail.com (Travis Beatty)
 */



/**
 * Bishop piece
 * @constructor
 * @extends {ChessPiece}
 */
function Bishop(position, color) {
    ChessPiece.call(this, position, color);
}
Bishop.prototype = new ChessPiece();
Bishop.prototype.constructor = Bishop;


/**
 * 3D model for us
 * @type {Object3D}
 */
Bishop.prototype.model = null;

Bishop.prototype.characternum = 9815;
