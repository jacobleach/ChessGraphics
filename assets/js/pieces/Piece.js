/*jshint globalstrict: true */
'use strict';

/**
 * @author travisby@gmail.com (Travis Beatty)
 */



/**
 * Individual piece to be moved on a coordinate system using {Position}
 * to describe a point
 * @interface
 */
function Piece() {}


/**
 * Moves a piece to position a new position.  ASSUMES MOVE IS LEGAL
 * @param {Position} position to move the Piece to
 */
Piece.prototype.move = function(position) {};

/**
 * File name for our 3D model for us
 * @type {Object3D}
 */
