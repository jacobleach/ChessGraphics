/*jshint globalstrict: true */
'use strict';

/**
 * @author travisby@gmail.com (Travis Beatty)
 */

//Feel free to change this
var BLACK_MATERIAL = 0;
var WHITE_MATERIAL = 1;
var pieceIsMoving = false;
/**
 * Individual piece to be moved on a Chess Board
 * @constructor
 * @implements {Piece}
 */
function ChessPiece(position, color) {

    this.color = color;

    // We do not want to clone object if we are just a ChessPiece
    if (this.constructor != ChessPiece) {
        this.model = this.toCopyModel.clone();
        this.model.traverse(
            function(object) {
                if(object instanceof THREE.Mesh) {
                    object.material = (color == BLACK_MATERIAL) ? ChessPiece.prototype.blackMaterial : ChessPiece.prototype.whiteMaterial;
                       object.castShadow = true;
                }
            }
        );

        if(color == BLACK_MATERIAL) {
            this.model.rotateY(Math.PI);
        }

        // starts our piece at e3
        this.position = new ChessPosition('e', 4);
        this.model.position.x += .5;
        this.model.position.z += .5;

        this.moveTo(position);
        this.position = position;
    }
}

ChessPiece.prototype.updateMaterials = function() {
    var self = this;
    this.model.traverse(
        function(object) {
            if(object instanceof THREE.Mesh) {
                object.material = (self.color == BLACK_MATERIAL) ? ChessPiece.prototype.blackMaterial : ChessPiece.prototype.whiteMaterial;
            }
        }
    );
}

/**
 * @return {Vector3} of our location
 */
ChessPiece.prototype.getLocation = function() {
  return this.model.position;
};


/**
 * @param {Vector3} vector to translate
 */
ChessPiece.prototype.translate = function(vector) {
    this.model.position.x = vector.x;
    this.model.position.y = vector.y;
    this.model.position.z = vector.z;
};

/**
 * TODO:
 */
ChessPiece.prototype.blackMaterial = null;

/**
 * TODO:
 */
ChessPiece.prototype.whiteMaterial = null;

/**
 * 3D model for us
 * @type {Object3D}
 */
ChessPiece.prototype.model = null;

ChessPiece.prototype.toCopyModel = null;

/**
 * Chess Position for where we are located
 * @type {ChessPosition}
 */
ChessPiece.prototype.position = null;

ChessPiece.prototype.color = 0;

/**
 * Determines the boolean color of this piece
 * @returns {boolean} is this a black piece?
 */
ChessPiece.prototype.isBlack = function () {
    return (this.color == BLACK_MATERIAL);
};


/**
 * Move to a particular spot on the chess board
 * @type {ChessPosition}
 */
ChessPiece.prototype.slideTo = function(position) {
    var temp = new THREE.Vector3(this.model.position.x + this.position.getDisplacementX(position), this.position.y, this.model.position.z + this.position.getDisplacementY(position));
    chessGame.animationHandler.add(new Slide(this, temp));
    this.position = position;
};

/**
 * Move to a particular spot on the chess board
 * @type {ChessPosition}
 */
ChessPiece.prototype.animateTo = function(position) {
    var temp = new THREE.Vector3(this.model.position.x + this.position.getDisplacementX(position), this.position.y, this.model.position.z + this.position.getDisplacementY(position));
    chessGame.animationHandler.add(new RiseAndFall(this, temp));
    this.position = position;
};
/**
 * Move to a particular spot on the chess board
 * @type {ChessPosition}
 */
ChessPiece.prototype.moveTo = function(position) {
    var temp = new THREE.Vector3(this.model.position.x + this.position.getDisplacementX(position), this.position.y, this.model.position.z + this.position.getDisplacementY(position));
    chessGame.animationHandler.add(new Teleport(this, temp));
    this.position = position;
}; 

ChessPiece.prototype.getChar = function () {
    var s = this.characternum;
    if (this.isBlack()) {
        s += 6;
    }
    return String.fromCharCode(s);
};
