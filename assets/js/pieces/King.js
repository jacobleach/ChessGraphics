/*jshint globalstrict: true */
'use strict';

/**
 * @author travisby@gmail.com (Travis Beatty)
 */



/**
 * King piece
 * @constructor
 * @extends {ChessPiece}
 */
function King(position, color) {
    ChessPiece.call(this, position, color);
}
King.prototype = new ChessPiece();
King.prototype.constructor = King;


/**
 * 3D model for us
 * @type {Object3D}
 */
King.prototype.model = null;

King.prototype.characternum = 9812;
