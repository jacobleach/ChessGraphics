/*jshint globalstrict: true */
'use strict';

/**
 * @author travisby@gmail.com (Travis Beatty)
 */



/**
 * Queen piece
 * @constructor
 * @extends {ChessPiece}
 */
function Queen(position, color) {
    ChessPiece.call(this, position, color);
}
Queen.prototype = new ChessPiece();
Queen.prototype.constructor = Queen;


/**
 * 3D model for us
 * @type {Object3D}
 */
Queen.prototype.model = null;

Queen.prototype.characternum = 9813;
