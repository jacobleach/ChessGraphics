/*jshint globalstrict: true */
'use strict';

/**
 * @author travisby@gmail.com (Travis Beatty)
 */



/**
 * Knight piece
 * @constructor
 * @extends {ChessPiece}
 */
function Knight(position, color) {
    ChessPiece.call(this, position, color);
}
Knight.prototype = new ChessPiece();
Knight.prototype.constructor = Knight;


/**
 * 3D model for us
 * @type {Object3D}
 */
Knight.prototype.model = null;

Knight.prototype.characternum = 9816;
