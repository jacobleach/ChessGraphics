/*jshint globalstrict: true */
'use strict';

/**
 * @author travisby@gmail.com (Travis Beatty)
 */



/**
 * Pawn piece
 * @constructor
 * @extends {ChessPiece}
 */
function Pawn(position, color) {
    ChessPiece.call(this, position, color);
}
Pawn.prototype = new ChessPiece();
Pawn.prototype.constructor = Pawn;


/**
 * 3D model for us
 * @type {Object3D}
 */
Pawn.prototype.model = null;

Pawn.prototype.characternum = 9817;
