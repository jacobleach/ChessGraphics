/*jshint globalstrict: true */
'use strict';

function loadModels(directoryURL) {

    var loader = new THREE.OBJLoader();
        // Pawn
        loader.load(
            directoryURL + "pawn.obj",
            function(object) {
                Pawn.prototype.toCopyModel = object;
                // King
                loader.load(
                    directoryURL + "king.obj",
                    function(object) {
                        King.prototype.toCopyModel = object;
                        // Queen
                        loader.load(
                            directoryURL + "queen.obj",
                            function(object) {
                                Queen.prototype.toCopyModel = object;
                                // Rook
                                loader.load(
                                    directoryURL + "rook.obj",
                                    function(object) {
                                        Rook.prototype.toCopyModel = object;
                                        // Knight
                                        loader.load(
                                            directoryURL + "knight.obj",
                                            function(object) {
                                                Knight.prototype.toCopyModel = object;
                                                // Bishop
                                                loader.load(
                                                    directoryURL + "bishop.obj",
                                                    function(object) {
                                                        Bishop.prototype.toCopyModel = object;
                                                        window.chessBoard.chessBoard.reload();
                                                    }
                                                );
                                            }
                                        );
                                    }
                                );
                            }
                        );
                    }
                );
            }
        );
}