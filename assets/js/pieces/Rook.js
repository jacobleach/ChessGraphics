/*jshint globalstrict: true */
'use strict';

/**
 * @author travisby@gmail.com (Travis Beatty)
 */



/**
 * Rook piece
 * @constructor
 * @extends {ChessPiece}
 */
function Rook(position, color) {
    ChessPiece.call(this, position, color);
}
Rook.prototype = new ChessPiece();
Rook.prototype.constructor = Rook;


/**
 * 3D model for us
 * @type {Object3D}
 */
Rook.prototype.model = null;

Rook.prototype.characternum = 9814;
