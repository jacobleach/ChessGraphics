/**
 * @author leach.jacob@gmail.com (Jacob Leach)
 *
 */

const WHITE = 1;
const BLACK = 2;
const SIDE = 3;

function ChessCameraFromConfigFactory() {
    var camera = new ChessCamera(
                    config.camera.fov,
                    config.camera.aspect,
                    config.camera.near,
                    config.camera.far
                );
    camera.camera.position = config.camera.initialPosition;
    camera.camera.lookAt(config.camera.initialLookAt);

    return camera;
}

/**
 * A camera object used to look upon the game.
 * @constructor
 *
 */
function ChessCamera(fov, aspect, near, far) {
    this.view = WHITE;
    this.overview = false;
    this.camera = new THREE.PerspectiveCamera(
        fov,
        aspect,
        near,
        far
    );
    this.controls = new THREE.OrbitControls( this.camera );
    this.controls.target.set( 0, 0, 0 )
};

/**
 *
 * @param {Integer} which side you are one given by the constants WHITE, BLACK, and SIDE.
 */
ChessCamera.prototype.moveToSide = function(view) {
    //If we are already on the view and we are not in overview mode, do nothing.

    if(this.overview) {
        console.log("FUCK YOU");
        if(view == WHITE) {
            this.camera.position = config.camera.whiteView;
        }
        else if(view == BLACK) {
            this.camera.position = config.camera.blackView;
        }

        this.overview = false;
        this.camera.lookAt(config.camera.initialLookAt);
    }

   if(!(this.view == view)) {

        if(view == WHITE) {
            window.chessGame.animationHandler.add(new Spin(window.chessGame.chessBoard.model, Math.PI));
        }
        else if(view == BLACK) {
            window.chessGame.animationHandler.add(new Spin(window.chessGame.chessBoard.model, -Math.PI));
        }
        else {
            this.camera.position  = config.camera.sideView;
        }

        this.camera.lookAt(config.camera.initialLookAt);
        this.view = view;
    }
}

/**
 * Moves the camera to look down upon the game.
 *
 */
ChessCamera.prototype.overview = function() {

    if(this.overview) {
        this.overview = false;
        this.camera.position = config.camera.initialPosition;
        this.controls = new THREE.TrackballControls( camera );
        this.controls.target.set( 0, 0, 0 )
    }

    this.overview = true;
    this.camera.position = config.camera.overview;
    this.camera.lookAt(config.camera.initialLookAt);
}

ChessCamera.prototype.getCamera = function() {
    return this.camera;
}