/*jshint globalstrict: true */
'use strict';

/**
 * @author leach.jacob@gmail.com (Jacob Leach)
 */

var setBoardMaterial = function(url) {

    var boardLoader = new THREE.OBJMTLLoader();

    boardLoader.load(
        config.models.boardObject,
        url,
        function(object) {
            ChessBoard.prototype.model = object;
            window.chessGame.chessBoard.updateBoard();
        }

    );
}

var setBlackMaterial = function(url) {

    //MTLLoader parameter is copied from THREE.JS source.
    var materialLoader = new THREE.MTLLoader(url.substr( 0, url.lastIndexOf( "/" ) + 1 ));

    var self = this;

    materialLoader.load(
        url,
        function(materials) {
            ChessPiece.prototype.blackMaterial = materials.create( "Material" );
            window.chessGame.chessBoard.updateAll();
        }
    );
}

var setWhiteMaterial = function(url) {

    //MTLLoader parameter is copied from THREE.JS source.
    var materialLoader = new THREE.MTLLoader(url.substr( 0, url.lastIndexOf( "/" ) + 1 ));

    var self = this;

    materialLoader.load(
        url,
        function(materials) {
            ChessPiece.prototype.whiteMaterial = materials.create( "Material" );
            window.chessGame.chessBoard.updateAll();
        }
    );
}

/**
 * A theme that contains models and materials for the chess board and pieces.
 */
function ChessTheme() {

    //Materials for pieces
    this.whiteMaterials = [];
    this.blackMaterials = [];

    //Objects for pieces
    this.pawnObject = null;
    this.rookObject = null;
    this.bishopObject = null;
    this.kingObject = null;
    this.queenObject = null;
    this.knightObject = null;

    //Board Material
    this.boardMaterial = null;

    //Board object
    this.boardObject = null;
}

/**
 * @param {String} the url of the MTL file for the white materials
 */
ChessTheme.prototype.loadWhiteMaterial = function (url) {
    loadMaterial(url, this.setWhiteMaterials);
}

/**
 * @param {String} the url of the MTL file for the black materials
 */
ChessTheme.prototype.loadBlackMaterial = function (url) {
    loadMaterial(url, this.setBlackMaterials);
}

/**
 * @param {[THREE.Material]} the black materials
 */
ChessTheme.prototype.setBlackMaterials = function (materials) {
    this.blackMaterials = materials;
}

/**
 * @param {[THREE.Material]} the white materials
 */
ChessTheme.prototype.setWhiteMaterials = function (materials) {
    this.whiteMaterials = materials;
}

/**
 * @param {String} the url of the MTL file
 * @param {Function}
 * @return {[THREE.Material]} the loaded materials
 */
ChessTheme.prototype.loadMaterial = function (url, onLoad) {

    //MTLLoader parameter is copied from THREE.JS source.
    var materialLoader = new THREE.MTLLoader(url.substr( 0, url.lastIndexOf( "/" ) + 1 ));

    var self = this;

    materialLoader.load(
        url,
        function(materials) { onLoad(materials.preload()) }
    );
}