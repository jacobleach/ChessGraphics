/**
 * @author leach.jacob@gmail.com (Jacob Leach)
 */


/**
 * Configuration file for our Chess Game.
 */

var config = {
    models: {},
    lights: {},
    camera: {}
};

//Model Directory
config.models.directory  =    "assets/models/current/";
config.models.materialsDirectory  =    "assets/models/materials/";
/* 
 * Individual file names.
 * Do not change anything outside of the quotes.
 */
config.models.darkMaterial =    materialLoader("black0.mtl");
config.models.lightMaterial =   materialLoader("white0.mtl");
config.models.boardMaterial =   materialLoader("board0.mtl");

config.models.boardObject =     loader("board.obj");
config.models.pawnObject =      loader("pawn.obj");
config.models.knightObject =    loader("knight.obj");
config.models.kingObject =      loader("king.obj");
config.models.queenObject =     loader("queen.obj");
config.models.rookObject =      loader("rook.obj");
config.models.bishopObject =    loader("bishop.obj");

/*
 * TODO: Comment
 *
 */
config.lights.directional =     [ [ [ 40, 40, 40,], 0xAAAAAA, .7 ] ];  
 
/*
 * The ambient glow of the scene.
 * Comment out to turn off.
 */
config.lights.ambient =         0x444444;

/*
 * Parameters:
 * 1) Position  [x, y, z]
 * 2) Target    [x, y, z]
 * 3) Color     Hex
 * 4) Intensity 0 - 1.0
 * 5) Cast Shadows? True / False
 */  
config.lights.spot =            [ 	[[  5,  10,  5],  [  2, 0,  2],  0xFFFFFF, .1, true],
                                    [[ -5,  10, -5],  [ -2, 0, -2],  0xFFFFFF, .1, false],
                                    [[  5,  10, -5],  [  2, 0, -2],  0xFFFFFF, .1, false],
                                    [[ -5,  10,  5],  [ -2, 0,  2],  0xFFFFFF, .1, false]
	                             ];

config.camera.fov             = 60;
config.camera.aspect          = window.innerWidth / window.innerHeight;
config.camera.near            = 1;
config.camera.far             = 100;
config.camera.initialPosition = new THREE.Vector3(0, 4,  8); 
config.camera.initialLookAt   = new THREE.Vector3(0, 0,  0);
config.camera.overview        = new THREE.Vector3(0, 8,  0);
config.camera.whiteView       = new THREE.Vector3(0, 4,  8);
config.camera.blackView       = new THREE.Vector3(0, 4, -8);
config.camera.sideView        = new THREE.Vector3(6, 4,  0);
    
function materialLoader(fileName) {
    return config.models.materialsDirectory + fileName;
};


function loader(fileName) {
    return config.models.directory + fileName;
};
