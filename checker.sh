#!/bin/sh

java -jar /opt/closure-compiler-bin-0/lib/closure-compiler-bin.jar --warning_level=VERBOSE --third_party assets/js/vendor/three.js `find . | grep js | grep -v three\.js`
gjslint -r assets/js --strict --disable 0100
